% Created 2021-11-03 mer. 10:33
% Intended LaTeX compiler: pdflatex
\documentclass[presentation,bigger]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage[french]{babel}
\usepackage[normalem]{ulem}
\usepackage{minted}
\usetheme{default}
\author{{\large Christophe Pouzat} \\ \vspace{0.2cm}IRMA, Université de Strasboug et CNRS\\ \vspace{0.2cm} \texttt{christophe.pouzat@mah.unistra.fr}}
\date{Mini colloque 3Rs, Celphedia, Bordeaux, 3 novembre 2021}
\title{La Recherche Reproductible : C'est quoi ? Pourquoi en faire ? Comment ?}
\setbeamercovered{invisible}
\beamertemplatenavigationsymbolsempty
\hypersetup{
 pdfauthor={{\large Christophe Pouzat} \\ \vspace{0.2cm}IRMA, Université de Strasboug et CNRS\\ \vspace{0.2cm} \texttt{christophe.pouzat@mah.unistra.fr}},
 pdftitle={La Recherche Reproductible : C'est quoi ? Pourquoi en faire ? Comment ?},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.2 (Org mode 9.4.6)}, 
 pdflang={French}}
\begin{document}

\maketitle


\section{Introduction}
\label{sec:org7c1a236}
\begin{frame}[label={sec:orga7f6468}]{Qu'est-ce que la « recherche reproductible » ?}
\begin{itemize}
\item Pour faire « simple », c'est une approche qui cherche à diminuer
 l'écart entre un idéal : les résultats devraient être reproductibles \pause
 ; et la réalité : il est souvent difficile, même pour leurs auteurs,
 de reproduire des résultats publiés.\pause
\item Concrètement, c'est une démarche qui consiste à fournir aux lecteurs
d'articles, d'ouvrages, etc, l'ensemble des données et des programmes -- voir l'environnement logiciel complet -- 
\alert{accompagnés d'une description algorithmique de la façon dont les
programmes ont été appliqués aux données} pour obtenir les résultats
présentés.
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org039f6ee}]{Pourquoi en faire ?}
Arrivé là, deux questions sont souvent posées :
\begin{itemize}
\item Pourquoi s'embêter à rendre un travail reproductible (au sens précédent) si personne ne le demande ?
\item Super, mais comment fait-on ?
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org444246a}]{Une remarque}
\begin{itemize}
\item dans la pratique, ce qui est donc entendu ici par « reproduction » est
tout ce qui vient \emph{après} la collecte des données -- il serait donc plus juste de parler d'\alert{analyse reproductible des données} -- ;
\item mais comme l'approche requiert un \emph{accès libre} à celles-ci, elles
deviennent critiquables et comparables : \alert{un pas important vers une
reproductibilité des données elles-mêmes}.
\end{itemize}
\end{frame}

\section{Histoire courte}
\label{sec:org9cd1a26}
\begin{frame}[label={sec:org3c37b1d}]{Le \emph{Stanford Exploration Project}}
En 1992, Jon Claerbout et Martin Karrenbach \href{http://sepwww.stanford.edu/doku.php?id=sep:research:reproducible:seg92}{écrivent} :

\begin{quote}
{\color{orange}
Une révolution dans la formation et dans le transfert technologique résulte du mariage du traitement de texte et des interpréteurs en ligne de commande de type script. Ce mariage permet à un auteur d'associer à chaque légende de figure une étiquette référençant tout ce qui est nécessaire à la régénération de la figure : les données, les paramètres et les programmes. Ceci fournit une exemple concret de reproductibilité en science computationnelle. Notre expérience, au Stanford Exploration Project, montre que la préparation de ce type de document électronique ne demande pas beaucoup plus de travail que celui nécessaire à la préparation d'un rapport classique ; il faut juste tout archiver de façon systématique.
}
\end{quote}
\end{frame}


\begin{frame}[label={sec:org39a0b68}]{}
Communication dont la « substantifique mœlle » sera extraite par
\href{http://statweb.stanford.edu/\~wavelab/Wavelab\_850/wavelab.pdf}{Buckheit
et Donoho (1995)} qui écriront :

\vspace{1cm}

\begin{quote}
{\color{orange}
An article about computational science in a scientific publication is
\textbf{not} the scholarship itself, it is merely \textbf{advertising} of the
scholarship. The actual scholarship is the complete software development
environment and the complete set of instructions which generated the
figures.}
\end{quote}
\end{frame}


\begin{frame}[label={sec:org1a61167},fragile]{Les outils du \emph{Stanford Exploration Project}}
 Les géophysiciens du SEP effectuent l'analyse de gros jeux de données
ainsi que des simulations de modèles géophysiques « compliqués » (basés
sur des EDPs) ; ainsi :

\begin{itemize}
\item<1-> ils ont l'habitude des langages compilés comme le
\texttt{FORTRAN} et le \texttt{C} ;
\item<1-> ils emploient des \href{https://fr.wikipedia.org/wiki/Moteur\_de\_production}{moteurs de production} (\emph{workflows}) comme \href{https://fr.wikipedia.org/wiki/GNU\_Make}{\texttt{Make}} ;
\item<1-> ils écrivent leurs articles en \(\TeX{}\) et \(\LaTeX{}\) ;
\item<2-> leur idée clé est d'utiliser le moteur de production, non seulement
pour générer les « exécutables », mais aussi pour les appliquer aux
données -- et ainsi générer les figures et les tables de l'article --,
avant de compiler le fichier \texttt{.tex}.
\end{itemize}
\end{frame}


\begin{frame}[label={sec:org3ce421c}]{Points forts et faibles de l'approche}
Points forts :
\begin{itemize}
\item \alert{tout} (données, codes sources, scripts, texte) est conservé dans une
collection de répertoires imbriqués ce qui rend le travail « facile »
à \alert{sauvegarder} et à \alert{distribuer} ;
\item un accent est mis dès le départ sur l'utilisation de logiciels
« libres ».
\end{itemize}

Points faibles :
\begin{itemize}
\item l'emploi de \(\TeX{}\) (ou \(\LaTeX{}\)) se prête mal à la « prise de
notes » et est un véritable obstacle hors des maths et de la
physique ;
\item la gestion d'une arborisation de fichiers, pour ne pas dire l'ensemble
de l'approche, est « lourde » dans le cadre d'une analyse exploratoire
« au quotidien ».
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org8e92d23},fragile]{Les langages de balisage léger}
 Un point « faible » de l'approche précédente, la nécessité d'écrire en \(\LaTeX{}\) ou \texttt{HTML}, a maintenant disparu avec le développement de \href{http://fr.wikipedia.org/wiki/Langage\_de\_balisage\_l\%C3\%A9ger}{langages de balisage léger} comme :

\begin{itemize}
\item \href{http://daringfireball.net/projects/markdown/}{\texttt{Markdown}} ;
\item \href{http://docutils.sourceforge.net/rst.html}{\texttt{reStructuredText}} ;
\item \href{http://asciidoc.org/}{\texttt{Asciidoc}} ;
\item \href{http://orgmode.org/fr/index.html}{\texttt{Org mode}} (utilisé pour préparer cette présentation).
\end{itemize}
\end{frame}


\begin{frame}[label={sec:org2f9c30b},fragile]{Exemple (version \texttt{R Markdown})}
 Un morceau de fichier \texttt{R Markdown} ressemble à :

\vspace{1.0cm}

\begin{minted}{md}
## Résumé des données
Le _résumé à 5 nombres_ du jeu de données `jeu_A`
est :

```{r resume-jeu_A}
summary(jeu_A)
```

On voit qu'__aucune saturation ne semble présente__...
\end{minted}

\vspace{1.0cm}
\pause
\alert{Un point important : nous avons ici affaire à des fichiers « textes » (format UTF-8)}.
\end{frame}

\begin{frame}[label={sec:org43a0945},fragile]{Petit comparatif}
 Les solutions suivantes permettent toutes à quiconque maîtrise déjà \texttt{R},
\texttt{Python}, \texttt{Julia}, d'être productif rapidement dans le cadre d'un
travail « exploratoire » ou interactif :

\begin{itemize}
\item Le « carnet de notes » (\emph{notebook})  \href{https://jupyter.org/}{\texttt{jupyter}} permet d'utiliser \emph{séparément} les trois langages ci-dessus (et même 37 autres), un défaut important de mon point de vue : il n'y a quasiment pas d'aide d'édition ;
\item \href{http://rmarkdown.rstudio.com/}{RMarkdown} utilisé avec \href{https://www.rstudio.com/}{RStudio} permet de mettre en œuvre facilement la recherche reproductible avec \texttt{R} et (un peu) avec \texttt{Python} (avec une bonne aide d'édition).
\end{itemize}
\end{frame}

\section{Aller plus loin}
\label{sec:org958b7be}

\begin{frame}[label={sec:org590cfe7}]{Gestion de version}
Fidèles à la tradition de « détournement » des outils de développements logiciels pour faire de la recherche reproductible, ses praticiens deviennent souvent « dépendants » des logiciels de \alert{gestion de version} qui permettent de faire évoluer des documents tout en gardant une trace explicite de leurs différentes versions :
\vspace{1cm}
\begin{itemize}
\item \href{https://git-scm.com/}{git} devient de fait l'outil standard ;
\item avec \href{https://github.com/}{github} et \href{https://about.gitlab.com/}{gitlab}, même des « non-experts » arrivent à l'utiliser.
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org65c8237}]{Gros jeu de données}
Lorsque nous commençons à travailler sur de « vraies » données nous nous trouvons généralement confrontés à deux problèmes : 
\vspace{0.5cm}

\begin{itemize}
\item les données sont de nature « diverse », avec par exemple des enregistrements de potentiel membranaire \alert{et} une séquence d'images de fluorescence pour estimer la concentration calcique.
\item les données occupent un grand espace mémoire.
\end{itemize}
\end{frame}

\begin{frame}[label={sec:orgeff7f9c}]{Les métadonnées}
\begin{itemize}
\item Le format texte permet de stocker les données \alert{et} tout le reste\ldots{}
\item \(\Rightarrow\) ajouter des informations sur les données :
\begin{itemize}
\item provenance ;
\item date d'enregistrement ;
\item protocole d'acquisition ;
\item etc.
\end{itemize}
\item Ces informations sur les données sont ce qu'on appelle les \alert{métadonnées}.
\item Elles sont vitales pour la mise en œuvre de la recherche reproductible.
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org5fb4915},fragile]{Des formats binaires, pour données composites, permettant la sauvegarde de métadonnées}
 \begin{itemize}
\item Le \alert{Flexible Image Transport System} (\texttt{FITS}), créé en 1981 est toujours régulièrement mis à jour.
\item Le \alert{Hierarchical Data Format} (\texttt{HDF}), développé au \alert{National Center for Supercomputing Applications}, en est à sa cinquième version, \texttt{HDF5}.
\end{itemize}
\end{frame}


\begin{frame}[label={sec:org0e848d2}]{Les dépôts de données}
Le chercheur qui travaille sur des données expérimentales (par
opposition à des simulations) risque tôt ou tard d'avoir un problème lié
à la recherche reproductible : comment rendre de gros jeux de données
accessibles / téléchargeables par quiconque ? Heureusement, de nombreux
dépôts publiques (et gratuits) sont apparus ces dernières années :
\vspace{0.5cm}
\begin{itemize}
\item \href{http://www.runmycode.org}{RunMyCode} ;
\item \href{https://zenodo.org}{Zenodo} ;
\item L'\href{https://osf.io}{Open Science Framework} ;
\item \href{http://figshare.com}{Figshare} ;
\item \href{http://datadryad.org/}{DRYAD} ;
\item \href{http://www.execandshare.org}{Exec\&Share}.
\end{itemize}
\end{frame}

\section{Prolèmes en perspective}
\label{sec:org0478bf1}

\begin{frame}[label={sec:org1a60a17},fragile]{Évolutions récentes}
 \begin{itemize}
\item Depuis une vingtaine d'années des langages de « haut niveau », interprétés, comme \texttt{Python}, \texttt{R} ou \texttt{Julia} se sont généralisés.
\item Certains comme \texttt{Python} évoluent très (trop) vite, ce qui pose de \alert{gros problèmes de stabilité dans la durée} car les développeurs ne se préoccupent pas vraiment de la rétro-compatibilité.
\item Les chaînes de traitement de données se sont complexifiées, de même que l'environnement matériel d'exécution : on développe les codes sur un portable et on les utilise « en grand » sur un cluster \(\Rightarrow\) les \emph{workflows} et autres \emph{pipelines} comme \href{https://snakemake.github.io/}{\texttt{snakemake}} sont en développement rapide.
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org9112354}]{Reproductibilité dans la durée}
\begin{itemize}
\item Une expérience souvent désagréable attend le chercheur qui se lance dans la recherche reproductible.
\item Malgré un document dynamique préparé avec le plus grand soin et permettant effectivement de régénérer une étude complète \emph{au moment de la création du document}, cette régénération échoue six mois ou deux ans après.
\item Cet échec résulte de la non prise en compte de la dépendance des résultats (numériques) de l'étude vis à vis de l'environnement logiciel dans lequel celle-ci a été effectuée.
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org529e8e9},fragile]{Solutions ?}
 \begin{itemize}
\item C'est encore un domaine où la recherche est active ce qui implique que le paysage change vite !
\item On peut continuer à travailler avec un langage de « haut niveau » comme \texttt{Python} et \alert{figer} sa pile logicielle au moyen d'un conteneur comme  \href{https://numpy.org/doc/stable/index.html}{Docker} ou \href{https://singularity.hpcng.org/}{Singularity}.
\item On peut décider de travailler au maximum avec des languages compilés et \alert{normalisés} comme le \texttt{C}, le \texttt{C++} ou le \texttt{Fortran} : ils évoluent moins vite et les nouvelles versions sont rétro-compatibles.
\end{itemize}
\end{frame}


\begin{frame}[label={sec:org00e96f1}]{Conclusions}
\begin{itemize}
\item Mettre en œuvre une recherche reproductible « au quotidien » ne présente plus aujourd'hui de gros problèmes.
\item La recherche reproductible est une approche jeune qui doit faire face à des problèmes pas toujours pleinement anticipés, comme la \emph{reproductibilité dans la durée}.
\item Comme toute discipline nouvelle et dynamique elle voit se présenter de nombreuses propositions de solutions, pas toujours compatibles, au problème rencontrés. Nous avons ainsi aujourd'hui de nombreux moteurs de \emph{workflow} à disposition, plusieurs systèmes de conteneurs, etc.
\item Comme l'un des enjeux majeurs est la fiabilité dans le temps il va nous falloir nécessairement faire preuve de patience et rester ouverts.
\end{itemize}
\end{frame}

\begin{frame}[label={sec:orgc8ab9c4},fragile]{Remerciements}
 \begin{itemize}
\item Les organisateurs du mini colloque « Comment améliorer le respect de la règle des 3Rs ? » pour m'avoir invité ;
\item mon employeur, le \texttt{CNRS}, qui me permet de m'embêter à rendre mon travail reproductible même si personne ne me le demande ;
\item les développeurs de tous les logiciels (libres) mentionnés dans cet exposé ainsi que ceux des logiciels que j'ai injustement oubliés ;
\item vous pour m'avoir écouté.
\end{itemize}
\end{frame}

\begin{frame}[label={sec:orgceefe78}]{Quelques références}
\begin{itemize}
\item Le CLOM/MOOC \href{https://learninglab.inria.fr/mooc-recherche-reproductible-principes-methodologiques-pour-une-science-transparente/}{Recherche reproductible : principes méthodologiques pour une science transparente}, évidemment !
\item \emph{Implementing Reproducible Research}, un livre édité par V Stodden, F
Leisch et R Peng, entièrement (et légalement) disponible sur le
\href{https://osf.io/s9tya/}{web}.
\item \href{http://faculty.washington.edu/rjl/talks/LeVeque\_CSE2011.pdf}{\emph{Top 10  Reasons to Not Share Your Code (and why you should anyway)}} une présentation de Randy LeVeque, à la fois très drôle et profonde.
\item Ricardo Wurmus a donné une \href{https://fosdem.org/2021/schedule/event/guix\_workflow/}{présentation} courte et lumineuse des enjeux et problèmes des \emph{workflows} dans le cadre de la recherche reproductible au FOSDEM 2021
\end{itemize}
\end{frame}
\end{document}